#!/bin/bash
fechaHoraActual=$(date +%d-%m-%Y_%H-%M-%S)

read -p "Ingrese la ruta del archivo o directorio a respaldar: " ruta

filename=${ruta}_${fechaHoraActual}
if [ -e $ruta ]
then

  if [ -f $ruta ]
  then
    echo "La ruta ingresada es un archivo..."
    tar -czvf /tmp/${filename}.tar.gz ${ruta}
    echo "Respaldo exitoso!!!"
  fi

  if [ -d $ruta ]
  then
    echo "La ruta ingresada es un directorio..."
    tar -czvf /tmp/$filename.tar.gz $ruta
    echo "Respaldo exitoso!!!"
  fi
else
  echo "El archivo o directorio especificado no existe..."
fi

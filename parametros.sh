#!/bin/bash
###Variables
#Parametro posicional del script
entrada="$1"
fecha=$(date +%s)

###Exit codes
#3 --> No es un archivo o directorio...

function valida_archivo(){
	#Parametro posicional de la funcion
	#entrada="$1"
	
	#Valida si la entrada esta vacia
	if [ -z $entrada ];then
		echo "Debe de escribir una ruta o nombre de archivo..."
	#Valida si es un archivo regular y existe
	elif [ -f $entrada ];then
		echo "Es un archivo"
		limpiarRuta
		comprime
	#Valida si es un directorio y existe
	elif [ -d $entrada ];then
		echo "Es un directorio"
		limpiarRuta
		comprime
	else
	#imprime codigo de salida
		echo "Error inesperado"
		exit 3
	fi
}

function limpiarRuta(){
	data=$(basename $entrada)
	respaldo="${data}_${fecha}.tar.gz"
}

function comprime(){
	destino="/tmp/"
	echo Creando respaldo en /tmp...
	tar -czf ${destino}${respaldo} $entrada
}

valida_archivo
